import { TravelRuleTransactionSchema, validate } from '../index.js';
import { describe, it, expect } from 'vitest';

const mockPIIReference = 'QmPK1s3pNYLi9ERiq3BDxKa4XosgWwFRQUydHUtz4YgpqB';

const mockPerson = {
  naturalPerson: {
    name: [
      {
        nameIdentifier: [
          {
            primaryIdentifier: mockPIIReference,
            secondaryIdentifier: mockPIIReference,
            nameIdentifierType: mockPIIReference,
          },
        ],
        localNameIdentifier: [
          {
            primaryIdentifier: mockPIIReference,
            secondaryIdentifier: mockPIIReference,
            nameIdentifierType: mockPIIReference,
          },
        ],
        phoneticNameIdentifier: [
          {
            primaryIdentifier: mockPIIReference,
            secondaryIdentifier: mockPIIReference,
            nameIdentifierType: mockPIIReference,
          },
        ],
      },
    ],
    geographicAddress: [
      {
        addressType: mockPIIReference,
        department: mockPIIReference,
        subDepartment: mockPIIReference,
        streetName: mockPIIReference,
        buildingNumber: mockPIIReference,
        buildingName: mockPIIReference,
        floor: mockPIIReference,
        postBox: mockPIIReference,
        room: mockPIIReference,
        postCode: mockPIIReference,
        townName: mockPIIReference,
        townLocationName: mockPIIReference,
        districtName: mockPIIReference,
        countrySubDivision: mockPIIReference,
        addressLine: mockPIIReference,
        country: mockPIIReference,
      },
    ],
    nationalIdentification: {
      nationalIdentifier: mockPIIReference,
      nationalIdentifierType: mockPIIReference,
      countryOfIssue: mockPIIReference,
      registrationAuthority: mockPIIReference,
    },
    customerIdentification: mockPIIReference,
    dateAndPlaceOfBirth: {
      dateOfBirth: mockPIIReference,
      placeOfBirth: mockPIIReference,
    },
    countryOfResidence: mockPIIReference,
  },
  legalPerson: {
    name: [
      {
        nameIdentifier: [
          {
            primaryIdentifier: mockPIIReference,
            secondaryIdentifier: mockPIIReference,
            nameIdentifierType: mockPIIReference,
          },
        ],
        localNameIdentifier: [
          {
            primaryIdentifier: mockPIIReference,
            secondaryIdentifier: mockPIIReference,
            nameIdentifierType: mockPIIReference,
          },
        ],
        phoneticNameIdentifier: [
          {
            primaryIdentifier: mockPIIReference,
            secondaryIdentifier: mockPIIReference,
            nameIdentifierType: mockPIIReference,
          },
        ],
      },
    ],
    geographicAddress: [
      {
        addressType: mockPIIReference,
        department: mockPIIReference,
        subDepartment: mockPIIReference,
        streetName: mockPIIReference,
        buildingNumber: mockPIIReference,
        buildingName: mockPIIReference,
        floor: mockPIIReference,
        postBox: mockPIIReference,
        room: mockPIIReference,
        postCode: mockPIIReference,
        townName: mockPIIReference,
        townLocationName: mockPIIReference,
        districtName: mockPIIReference,
        countrySubDivision: mockPIIReference,
        addressLine: mockPIIReference,
        country: mockPIIReference,
      },
    ],
    customerNumber: mockPIIReference,
    nationalIdentification: {
      nationalIdentifier: mockPIIReference,
      nationalIdentifierType: mockPIIReference,
      countryOfIssue: mockPIIReference,
      registrationAuthority: mockPIIReference,
    },
    countryOfRegistration: mockPIIReference,
  },
};

describe('TravelRule Transaction', () => {
  it('should return valid for a full travel rule transaction', () => {
    const { valid, errors } = validate(TravelRuleTransactionSchema, {
      transactionId: 'eb9c2504-ecd6-42b9-a19c-78e9f04bf127',
      destinationAddress: '0x0000000000000000000000000000000000000000',
      assetType: 'eip155:1/slip44:60',
      amount: '1',
      pii: {
        originator: {
          originatorPersons: [mockPerson],
        },
        beneficiary: {
          beneficiaryPersons: [mockPerson],
        },
        transferPath: {
          transferPath: [
            {
              intermediaryVASP: mockPerson,
              sequence: mockPIIReference,
            },
          ],
        },
        payloadMetadata: {
          transliterationMethod: mockPIIReference,
        },
      },
    });

    expect(errors).toBe(null);
    expect(valid).toBe(true);
  });
});
