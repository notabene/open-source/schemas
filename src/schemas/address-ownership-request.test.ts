import { AddressOwnershipRequestSchema, validate } from '../index.js';
import { describe, it, expect } from 'vitest';

describe('Address ownership request', () => {
  it('should return valid for a valid object', () => {
    const { valid } = validate(AddressOwnershipRequestSchema, {
      transactionId: 'eb9c2504-ecd6-42b9-a19c-78e9f04bf127',
      address: '0xf877B383eB56Cd9fF1391A95d551e68F2168016E',
      asset: 'ETH',
    });

    expect(valid).toBe(true);
  });

  it('should return invalid or an invalid object', () => {
    const { valid } = validate(AddressOwnershipRequestSchema, {});

    expect(valid).toBe(false);
  });
});
