import { Schema } from 'ajv';

export const schema: Schema = {
  type: 'object',
  oneOf: [
    {
      properties: {
        transactionId: { type: 'string' },
        type: { type: 'string', enum: ['simple'] },
        own: { type: 'boolean' },
      },
      required: ['type', 'own', 'transactionId'],
      additionalProperties: false,
    },
    {
      properties: {
        transactionId: { type: 'string' },
        type: { type: 'string', enum: ['crypto'] },
        proof: { type: 'string' },
      },
      required: ['type', 'proof', 'transactionId'],
      additionalProperties: false,
    },
    {
      properties: {
        transactionId: { type: 'string' },
        type: { type: 'string', enum: ['unknown'] },
      },
      required: ['type', 'transactionId'],
      additionalProperties: false,
    },
  ],
};

type AddressOwnershipResponseSimple = {
  transactionId: string;
  type: 'simple';
  own: boolean;
};

type AddressOwnershipResponseCrypto = {
  transactionId: string;
  type: 'crypto';
  proof: string;
};

type AddressOwnershipResponseUnknown = {
  transactionId: string;
  type: 'unknown';
};

export type AddressOwnershipResponse =
  | AddressOwnershipResponseSimple
  | AddressOwnershipResponseCrypto
  | AddressOwnershipResponseUnknown;
