import { TravelRuleResolutionSchema, validate } from '../index.js';
import { describe, it, expect } from 'vitest';

describe('Travel rule resolution', () => {
  it('should return valid for a rejected resolution', () => {
    const { valid } = validate(TravelRuleResolutionSchema, {
      transactionId: 'eb9c2504-ecd6-42b9-a19c-78e9f04bf127',
      status: 'rejected',
      reason: 'beneficiary-mismatch',
    });

    expect(valid).toBe(true);
  });
  it('should return valid for a accepted resolution', () => {
    const { valid } = validate(TravelRuleResolutionSchema, {
      transactionId: 'eb9c2504-ecd6-42b9-a19c-78e9f04bf127',
      status: 'accepted',
    });

    expect(valid).toBe(true);
  });

  it('should return invalid or an invalid object', () => {
    const { valid } = validate(TravelRuleResolutionSchema, {
      status: 'accepted',
    });

    expect(valid).toBe(false);
  });
});
