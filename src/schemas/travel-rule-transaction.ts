import { Schema } from 'ajv';

const PIIReferenceSchema: Schema = {
  type: 'string',
};

const PIIReferenceSchemaArray: Schema = {
  type: 'array',
  items: {
    type: 'string',
  },
};

type PIIReference = string;

type NaturalPersonNameID = {
  primaryIdentifier?: PIIReference;
  secondaryIdentifier?: PIIReference;
  nameIdentifierType?: NaturalPersonNameTypeCode;
};

type LocalNaturalPersonNameID = {
  primaryIdentifier?: PIIReference;
  secondaryIdentifier?: PIIReference;
  nameIdentifierType?: NaturalPersonNameTypeCode;
};

type LegalPersonNameID = {
  legalPersonName?: PIIReference;
  legalPersonNameIdentifierType?: LegalPersonNameTypeCode;
};

type LocalLegalPersonNameID = {
  legalPersonName?: PIIReference;
  legalPersonNameIdentifierType?: LegalPersonNameTypeCode;
};

enum NaturalPersonNameTypeCode {
  ALIA = 'ALIA',
  BIRT = 'BIRT',
  MAID = 'MAID',
  LEGL = 'LEGL',
  MISC = 'MISC',
}

enum LegalPersonNameTypeCode {
  LEGL = 'LEGL',
  SHRT = 'SHRT',
  TRAD = 'TRAD',
}

const NaturalPersonNameIDSchema: Schema = {
  type: 'object',
  properties: {
    primaryIdentifier: PIIReferenceSchema,
    secondaryIdentifier: PIIReferenceSchema,
    nameIdentifierType: PIIReferenceSchema,
  },
};

const LegalPersonNameIDSchema: Schema = {
  type: 'object',
  properties: {
    legalPersonName: PIIReferenceSchema,
    legalPersonNameIdentifierType: PIIReferenceSchema,
  },
};

const AddressSchema: Schema = {
  type: 'object',
  properties: {
    addressType: PIIReferenceSchema,
    department: PIIReferenceSchema,
    subDepartment: PIIReferenceSchema,
    streetName: PIIReferenceSchema,
    buildingNumber: PIIReferenceSchema,
    buildingName: PIIReferenceSchema,
    floor: PIIReferenceSchema,
    postBox: PIIReferenceSchema,
    room: PIIReferenceSchema,
    postCode: PIIReferenceSchema,
    townName: PIIReferenceSchema,
    townLocationName: PIIReferenceSchema,
    districtName: PIIReferenceSchema,
    countrySubDivision: PIIReferenceSchema,
    addressLine: PIIReferenceSchema,
    country: PIIReferenceSchema,
  },
};

type Address = {
  addressType?: AddressTypeCode;
  department?: PIIReference;
  subDepartment?: PIIReference;
  streetName?: PIIReference;
  buildingNumber?: PIIReference;
  buildingName?: PIIReference;
  floor?: PIIReference;
  postBox?: PIIReference;
  room?: PIIReference;
  postCode?: PIIReference;
  townName?: PIIReference;
  townLocationName?: PIIReference;
  districtName?: PIIReference;
  countrySubDivision?: PIIReference;
  addressLine?: PIIReference[];
  country?: PIIReference;
};

enum AddressTypeCode {
  HOME = 'HOME',
  BIZZ = 'BIZZ',
  GEOG = 'GEOG',
}

const NaturalNameSchema: Schema = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      nameIdentifier: {
        type: 'array',
        items: NaturalPersonNameIDSchema,
      },
      localNameIdentifier: {
        type: 'array',
        items: NaturalPersonNameIDSchema,
      },
      phoneticNameIdentifier: {
        type: 'array',
        items: NaturalPersonNameIDSchema,
      },
    },
    required: ['nameIdentifier'],
  },
};

const LegalNameSchema: Schema = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      nameIdentifier: {
        type: 'array',
        items: LegalPersonNameIDSchema,
      },
      localNameIdentifier: {
        type: 'array',
        items: LegalPersonNameIDSchema,
      },
      phoneticNameIdentifier: {
        type: 'array',
        items: LegalPersonNameIDSchema,
      },
    },
    required: ['nameIdentifier'],
  },
};

type NaturalPersonName = {
  nameIdentifier?: NaturalPersonNameID[];
  localNameIdentifier?: LocalNaturalPersonNameID[];
  phoneticNameIdentifier?: LocalNaturalPersonNameID[];
};

type LegalPersonName = {
  nameIdentifier?: LegalPersonNameID[];
  localNameIdentifier?: LocalLegalPersonNameID[];
  phoneticNameIdentifier?: LocalLegalPersonNameID[];
};

const IVMS101Person: Schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    naturalPerson: {
      type: 'object',
      additionalProperties: false,
      properties: {
        name: NaturalNameSchema,
        geographicAddress: {
          type: 'array',
          items: AddressSchema,
        },
        nationalIdentification: {
          type: 'object',
          additionalProperties: false,
          properties: {
            nationalIdentifier: PIIReferenceSchema,
            nationalIdentifierType: PIIReferenceSchema,
            countryOfIssue: PIIReferenceSchema,
            registrationAuthority: PIIReferenceSchema,
          },
          required: ['nationalIdentifier', 'nationalIdentifierType'],
        },
        customerIdentification: PIIReferenceSchema,
        dateAndPlaceOfBirth: {
          type: 'object',
          additionalProperties: false,
          properties: {
            dateOfBirth: PIIReferenceSchema,
            placeOfBirth: PIIReferenceSchema,
          },
          required: ['dateOfBirth', 'placeOfBirth'],
        },
        countryOfResidence: PIIReferenceSchema,
      },
    },
    legalPerson: {
      type: 'object',
      additionalProperties: false,
      properties: {
        name: LegalNameSchema,
        geographicAddress: {
          type: 'array',
          items: AddressSchema,
        },
        customerNumber: PIIReferenceSchema,
        nationalIdentification: {
          type: 'object',
          additionalProperties: false,
          properties: {
            nationalIdentifier: PIIReferenceSchema,
            nationalIdentifierType: PIIReferenceSchema,
            countryOfIssue: PIIReferenceSchema,
            registrationAuthority: PIIReferenceSchema,
          },
          required: ['nationalIdentifier', 'nationalIdentifierType'],
        },
        countryOfRegistration: PIIReferenceSchema,
      },
    },
  },
};

type IVMS101Person = {
  naturalPerson?: {
    name?: NaturalPersonName[];
    geographicAddress?: Address[];
    nationalIdentification?: {
      nationalIdentifier?: PIIReference;
      nationalIdentifierType?: NationalIdentifierTypeCode;
      countryOfIssue?: PIIReference;
      registrationAuthority?: PIIReference;
    };
    customerIdentification?: PIIReference;
    dateAndPlaceOfBirth?: {
      dateOfBirth?: PIIReference;
      placeOfBirth?: PIIReference;
    };
    countryOfResidence?: PIIReference;
  };
  legalPerson?: {
    name?: LegalPersonName;
    geographicAddress?: Address[];
    customerNumber?: PIIReference;
    nationalIdentification?: {
      nationalIdentifier?: PIIReference;
      nationalIdentifierType?: PIIReference;
      countryOfIssue?: PIIReference;
      registrationAuthority?: PIIReference;
    };
    countryOfRegistration?: PIIReference;
  };
};

enum NationalIdentifierTypeCode {
  ARNU = 'ARNU',
  CCPT = 'CCPT',
  RAID = 'RAID',
  DRLC = 'DRLC',
  FIIN = 'FIIN',
  TXID = 'TXID',
  SOCS = 'SOCS',
  IDCD = 'IDCD',
  LEIX = 'LEIX',
  MISC = 'MISC',
}

export const schema: Schema = {
  type: 'object',
  properties: {
    transactionId: { type: 'string' },
    destinationAddress: { type: 'string' },
    assetType: { type: 'string' },
    amount: { type: 'string' },
    pii: {
      type: 'object',
      properties: {
        originator: {
          type: 'object',
          properties: {
            originatorPersons: {
              type: 'array',
              items: IVMS101Person,
            },
            accountNumber: PIIReferenceSchemaArray,
          },
          required: ['originatorPersons'],
        },
        beneficiary: {
          type: 'object',
          properties: {
            beneficiaryPersons: {
              type: 'array',
              items: IVMS101Person,
            },
            accountNumber: PIIReferenceSchemaArray,
          },
          required: ['beneficiaryPersons'],
        },
        transferPath: {
          type: 'object',
          properties: {
            transferPath: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  intermediaryVASP: IVMS101Person,
                  sequence: PIIReferenceSchema,
                },
                required: ['intermediaryVASP', 'sequence'],
              },
            },
          },
        },
        payloadMetadata: {
          type: 'object',
          properties: {
            transliterationMethod: PIIReferenceSchema,
          },
        },
      },
    },
    beneficiaryVP: {
      type: 'string',
    },
  },
  required: [
    'transactionId',
    'destinationAddress',
    'assetType',
    'amount',
    'pii',
  ],
  additionalProperties: false,
};

export type TravelRuleTransaction = {
  transactionId: string;
  destinationAddress: string;
  assetType: string;
  amount: string;
  pii: {
    originator: {
      originatorPersons: IVMS101Person[];
      accountNumber: PIIReference[];
    };
    beneficiary: {
      beneficiaryPersons: IVMS101Person[];
      accountNumber: PIIReference[];
    };
    transferPath?: {
      intermediaryVASP: IVMS101Person[];
      sequence: PIIReference;
    };
    payloadMetadata?: {
      transliterationMethod: PIIReference;
    };
  };
};
