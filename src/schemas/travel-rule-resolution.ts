import { Schema } from 'ajv';

export const schema: Schema = {
  type: 'object',
  properties: {
    transactionId: { type: 'string' },
    status: { type: 'string', enum: ['rejected', 'accepted'] },
    reason: { type: 'string' },
  },
  required: ['status', 'transactionId'],
  additionalProperties: false,
};

type TravelRuleResolutionAccepted = {
  transactionId: string;
  status: 'accepted';
};

type TravelRuleResolutionRejected = {
  transactionId: string;
  status: 'rejected';
  reason?: string;
};

export type TravelRuleResolution =
  | TravelRuleResolutionAccepted
  | TravelRuleResolutionRejected;
