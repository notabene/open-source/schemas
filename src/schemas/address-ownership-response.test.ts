import { AddressOwnershipResponseSchema, validate } from '../index.js';
import { describe, it, expect } from 'vitest';

describe('Address ownership response', () => {
  it('should return valid for a simple response', () => {
    const { valid } = validate(AddressOwnershipResponseSchema, {
      transactionId: 'eb9c2504-ecd6-42b9-a19c-78e9f04bf127',
      type: 'simple',
      own: true,
    });

    expect(valid).toBe(true);
  });

  it('should return valid for a crypto response', () => {
    const { valid } = validate(AddressOwnershipResponseSchema, {
      transactionId: 'eb9c2504-ecd6-42b9-a19c-78e9f04bf127',
      type: 'crypto',
      proof: '2YZbbyBkxWibKmW8mYH7',
    });

    expect(valid).toBe(true);
  });

  it('should return valid for an unknown response', () => {
    const { valid } = validate(AddressOwnershipResponseSchema, {
      transactionId: 'eb9c2504-ecd6-42b9-a19c-78e9f04bf127',
      type: 'unknown',
    });

    expect(valid).toBe(true);
  });

  it('should return invalid for an invalid simple response', () => {
    const { valid } = validate(AddressOwnershipResponseSchema, {
      type: 'simple',
    });

    expect(valid).toBe(false);
  });

  it('should return invalid for an invalid crypto response', () => {
    const { valid } = validate(AddressOwnershipResponseSchema, {
      type: 'crypto',
    });

    expect(valid).toBe(false);
  });

  it('should return invalid for an invalid unknown response', () => {
    const { valid } = validate(AddressOwnershipResponseSchema, {
      type: 'invalid',
      extra: 'field',
    });

    expect(valid).toBe(false);
  });
});
