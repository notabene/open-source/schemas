import { Schema } from 'ajv';

export const schema: Schema = {
  type: 'object',
  properties: {
    transactionId: { type: 'string' },
    address: { type: 'string' },
    asset: { type: 'string' },
  },
  required: ['address', 'asset', 'transactionId'],
  additionalProperties: false,
};

export type AddressOwnershipRequest = {
  transactionId: string;
  address: string;
  asset: string;
};
