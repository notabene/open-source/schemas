import AJV, { Schema } from 'ajv';
export {
  schema as AddressOwnershipRequestSchema,
  type AddressOwnershipRequest,
} from './schemas/address-ownership-request.js';
export {
  schema as AddressOwnershipResponseSchema,
  type AddressOwnershipResponse,
} from './schemas/address-ownership-response.js';
export {
  schema as TravelRuleResolutionSchema,
  type TravelRuleResolution,
} from './schemas/travel-rule-resolution.js';
export {
  schema as TravelRuleTransactionSchema,
  type TravelRuleTransaction,
} from './schemas/travel-rule-transaction.js';

const ajv = new AJV();

export const validate = (schema: Schema, data: unknown) => {
  const valid = ajv.validate(schema, data);
  return { valid, errors: ajv.errors };
};
