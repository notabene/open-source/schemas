# Schemas

## Description

This repository contains schemas that are contracts for messages passed within Notabene.

## Install

To use this package add the following to an `.npmrc` file:

```
//gitlab.com/api/v4/packages/npm/:_authToken=${GITLAB_AUTH_TOKEN}
@notabene:registry=https://gitlab.com/api/v4/packages/npm/
```

Then run `yarn add @notabene/schemas`

## Usage

```
import { validate, AddressOwnershipRequestSchema, AddressOwnershipRequest } from '@notabene/schemas';

const request: AddressOwnershipRequest = {
  address: '0xf877B383eB56Cd9fF1391A95d551e68F2168016E',
  asset: 'ETH',
}

validate(AddressOwnershipRequestSchema, request); // Returns true
```

## Contributing

- `yarn` - To install dependencies
- `yarn dlx @yarnpkg/sdks vscode` - Sets up VSCode to support ESLint/Prettier/TypeScript using Yarn 3 (make sure to install `ZipFS` vscode extension too)
- `yarn watch` - Hot reloading for development
- `yarn build` - Compile the library to the `dist/` folder
- `yarn lint` - Lints the library using ESLint/Prettier
- `yarn test` - Run Jest to test the library
- `yarn release` - Release the project using semantic-release (only run in CI)
